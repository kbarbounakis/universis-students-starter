# universis-students-starter

A starter project for building a mimic of [Universis Students](https://gitlab.com/universis/universis-students) by using latest tools and libraries of universis project. This application is a typical @angular/cli project. Read more about available command line tools [here](https://angular.io/cli)

![Screenshot](docs/img/Screenshot-Courses-1.png)

## Usage

Clone git repository

    git clone https://gitlab.com/kbarbounakis/universis-students-starter.git

install dependencies

    npm ci

and finally start development server

    npm start

