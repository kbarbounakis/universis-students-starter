import { createReducer, on } from '@ngrx/store';
import { LoadStudentCourses, LoadStudentCoursesSuccess } from './course.actions';
import * as fromSelectors from './course.selectors'

export const coursesFeature = 'courses';

export const coursesReducer = createReducer(
    fromSelectors.initialState,
    on(LoadStudentCourses, state => {
        return { ...state, items: state.items }
    }),
    on(LoadStudentCoursesSuccess, (state, { data }) => {
        return { ...state, items: data }
    })
  );