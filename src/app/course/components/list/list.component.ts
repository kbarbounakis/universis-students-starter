import { Component, AfterViewInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { LoadStudentCourses } from '../../course.actions';
import * as fromCourseSelectors from '../../../course/course.selectors'
import { Observable } from 'rxjs';
import { fadeAnimation } from '../../../animations';

@Component({
  selector: 'student-course-list',
  templateUrl: './list.component.html',
  styles: [
  ],
  animations: [
    fadeAnimation
  ]
})
export class ListComponent implements AfterViewInit {
  
  courses$: Observable<any[]>;

  constructor(private store: Store<{courses: any[]}>) {
    this.courses$ = this.store.pipe(select(fromCourseSelectors.selectCourseItems));
  }
  
  ngAfterViewInit(): void {
    this.store.dispatch({ type: LoadStudentCourses.type });
  }
}
