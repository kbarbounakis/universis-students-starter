import { Component, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as fromCourseSelectors from '../../../course/course.selectors'
import { Subscription } from 'rxjs';
import { ChartConfiguration, ChartType } from 'chart.js';

@Component({
  selector: 'ects-doughnut',
  template: `
    <div class="card">
      <div class="card-header mt-2 text-center">
        <span class="h3 font-weight-normal">ECTS</span>
      </div>
      <div class="card-body">
      <canvas baseChart
        [labels]="doughnutChartLabels"
        [datasets]="doughnutChartDatasets"
        [legend]="false"
        [type]="doughnutChartType">
</canvas>
      </div>
  </div>
  `
})
export class EctsDoughnutComponent implements OnDestroy {
  subscription: Subscription;
  public doughnutChartLabels: string[] = ['Passed', 'Failed'];
  public doughnutChartType: ChartType = 'doughnut';
  public doughnutChartDatasets: ChartConfiguration<'doughnut'>['data']['datasets'] = [{
    data: [
      2,
      10
    ],
    backgroundColor: [
      'Azure', 'AliceBlue'
    ]
  }];
  constructor(private store: Store<{courses: any[]}>) {
    this.subscription = this.store.pipe(select(fromCourseSelectors.selectCourseItems)).subscribe((items) => {
      this.doughnutChartDatasets = [{
        data: [
          items.filter((x: { isPassed: boolean, ects: number }) => x.isPassed).map((x) => x.ects)
            .reduce((previous, current) => {
              previous += current;
              return previous
            }),
            items.filter((x: { isPassed: boolean, ects: number }) => !x.isPassed).map((x) => x.ects)
            .reduce((previous, current) => {
              previous += current;
              return previous
            }),
        ],
        backgroundColor: [
          'MediumAquaMarine', 'LightCoral'
        ]
      }]
    });
  }
  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe()
    }
  }
}
