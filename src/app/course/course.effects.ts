import { Injectable } from '@angular/core';
import { Actions, CreateEffectMetadata, createEffect, ofType } from '@ngrx/effects';
import { LoadStudentCourses, LoadStudentCoursesSuccess, LoadStudentCoursesFailure } from './course.actions';
import * as fromActions from './course.actions'
import { catchError, exhaustMap, map, from, of, Observable, switchMap, take } from 'rxjs';
import { AngularDataContext } from '@themost/angular';


@Injectable()
export class StudentCourseEffects {

  loadStudentCourses$: Observable<{ type: "[StudentCourse] Load StudentCourses Success"; data: any; } | { type: "[StudentCourse] Load StudentCourses Failure"; }> & CreateEffectMetadata;

  constructor(private actions$: Actions, private context: AngularDataContext) {
    this.loadStudentCourses$ = createEffect(() => this.actions$.pipe(
      ofType(LoadStudentCourses.type),
      exhaustMap(() => from(this.context.model('students/me/courses').getItems())
        .pipe(
          map(results => ({ type: LoadStudentCoursesSuccess.type, data: results })),
          catchError(() => of({ type: LoadStudentCoursesFailure.type }))
        )),
        take(1)
      )
    );
  }
}
