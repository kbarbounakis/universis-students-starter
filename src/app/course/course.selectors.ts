import { createSelector, createFeatureSelector } from '@ngrx/store';

export interface CourseState {
    items: any[];
}

export const initialState: CourseState = {
    items: []
};

export const selectCourses = createFeatureSelector('courses')

export const selectCourseItems = createSelector(
    selectCourses,
    (state: CourseState) => {
        return state.items;
    }
);

export const selectPassedCourses = createSelector(
    selectCourses,
    (state: CourseState) => {
        return state.items.filter((item: { isPassed: boolean }) => item.isPassed);
    }
);