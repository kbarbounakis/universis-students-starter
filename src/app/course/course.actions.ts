import { createAction, props } from '@ngrx/store';

export const LoadStudentCourses = createAction(
  '[StudentCourse] Load StudentCourses'
);

export const LoadStudentCoursesSuccess = createAction(
  '[StudentCourse] Load StudentCourses Success',
  props<{ data: any }>()
);

export const LoadStudentCoursesFailure = createAction(
  '[StudentCourse] Load StudentCourses Failure',
  props<{ error: any }>()
);
