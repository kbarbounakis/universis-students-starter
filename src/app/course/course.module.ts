import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { StudentCourseEffects } from './course.effects';
import { StoreModule } from '@ngrx/store';
import * as fromReducers from './course.reducers';
import { CourseDoughnutComponent } from './components/course-doughnut/course-doughnut.component'
import { NgChartsModule } from 'ng2-charts';
import { EctsDoughnutComponent } from './components/course-doughnut/ects-doughnut.component';
import { IndexComponent } from './components/index/index.component';
import { ListComponent } from './components/list/list.component';
import { CourseRoutingModule } from './course-routing.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CourseDoughnutComponent,
    EctsDoughnutComponent,
    IndexComponent,
    ListComponent
  ],
  exports: [
    CourseDoughnutComponent,
    EctsDoughnutComponent
  ],
  imports: [
    CommonModule,
    StoreModule,
    NgChartsModule,
    CourseRoutingModule,
    StoreModule.forFeature(fromReducers.coursesFeature, fromReducers.coursesReducer),
    EffectsModule,
    EffectsModule.forFeature([StudentCourseEffects]),
    TranslateModule
  ]
})
export class CourseModule {
  static forRoot(): ModuleWithProviders<CourseModule> {
    return {
      ngModule: CourseModule,
      providers: [
      ]
    };
  }
}
