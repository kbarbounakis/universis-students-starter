import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from "@universis/common";
import { FullLayoutComponent } from "./layout/full-layout.component";
export const routes: Routes = [
  {
    path: "",
    redirectTo: "home",
    pathMatch: "full",
  },
  {
    path: "",
    component: FullLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: "Root",
    },
    children: [
      {
        path: 'home',
        data: {
          title: "Home",
        },
        loadChildren: () => import('./home/home.module').then((m) => m.HomeModule)
      },
      {
        path: 'courses',
        data: {
          title: "Student Courses",
        },
        loadChildren: () => import('./course/course.module').then((m) => m.CourseModule)
      },
      {
        path: 'registrations',
        data: {
          title: "Student Registrations",
        },
        loadChildren: () => import('./registration/registration.module').then((m) => m.RegistrationModule)
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
