import { BrowserModule } from "@angular/platform-browser";
import { NgModule, APP_INITIALIZER, LOCALE_ID } from "@angular/core";
import { AppComponent } from "./app.component";
import { RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { FullLayoutComponent } from "./layout/full-layout.component";
import { APP_LOCATIONS, ErrorModule, SharedModule } from "@universis/common";
import { AuthModule, ConfigurationService } from "@universis/common";
import { LocationStrategy, HashLocationStrategy, registerLocaleData } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { AngularDataContext, MostModule } from "@themost/angular";
import { AppRoutingModule } from "./app.routing-module";
import { ModalModule } from "ngx-bootstrap/modal";
import { THIS_APP_LOCATIONS } from './app.locations';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import el from '@angular/common/locales/el';
import en from '@angular/common/locales/en';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { EffectsModule } from '@ngrx/effects';
import { CourseModule } from "./course/course.module";

@NgModule({
  declarations: [AppComponent, FullLayoutComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    MostModule.forRoot({
      base: "/",
      options: {
        useMediaTypeExtensions: false,
        useResponseConversion: true,
      },
    }),
    SharedModule.forRoot(),
    ErrorModule.forRoot(),
    AuthModule.forRoot(),
    ModalModule.forRoot(),
    AppRoutingModule,
    StoreModule.forRoot(reducers, {
      metaReducers
    }),
    CourseModule.forRoot(),
    EffectsModule.forRoot([])
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      // use APP_INITIALIZER to load application configuration
      useFactory: (configurationService: ConfigurationService, context: AngularDataContext) =>
        async () => {
          // load application configuration
          await configurationService.load();
          context.setBase(configurationService.config.settings.remote.server);
          // load angular locales
          registerLocaleData(en);
          registerLocaleData(el);
          return true;
        },
      deps: [ConfigurationService, AngularDataContext],
      multi: true
    },
    {
      provide: APP_LOCATIONS,
      useValue: THIS_APP_LOCATIONS,
    },
    {
      provide: LOCALE_ID,
      useFactory: (configurationService: ConfigurationService) => {
        return configurationService.currentLocale;
      },
      deps: [ConfigurationService],
    },
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
