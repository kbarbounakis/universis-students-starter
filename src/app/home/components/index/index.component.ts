import { AfterViewInit, Component } from '@angular/core';
import { Store } from '@ngrx/store';
@Component({
  selector: 'home-index',
  templateUrl: './index.component.html'
})
export class IndexComponent implements AfterViewInit {

  constructor(private store: Store<{courses: any[]}>) {
  }
  
  ngAfterViewInit(): void {
  }
}
