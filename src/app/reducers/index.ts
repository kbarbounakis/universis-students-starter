import { isDevMode } from '@angular/core';
import {
  Action,
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { merge } from 'lodash';

function sessionStorageMergeMetaReducer<S, A extends Action = Action>(reducer: ActionReducer<S, A>) {
  let onInit = true;
  return function(state: S, action: A): S {
    const nextState = reducer(state, action);
    if (onInit === false) {
      return nextState
    }
    // init the application state.
    const savedState = JSON.parse(sessionStorage.getItem('students.state')) || {};
    if (onInit) {
      onInit  = false;
      return merge(nextState, savedState);
    }
    const setState = merge(savedState, nextState);
    sessionStorage.setItem('students.state', JSON.stringify(setState));
    return nextState;
  };
}

export interface AppState {
}

export const reducers: ActionReducerMap<AppState> = {  
};


export const metaReducers: MetaReducer<AppState>[] = isDevMode() ? [
] : [];
