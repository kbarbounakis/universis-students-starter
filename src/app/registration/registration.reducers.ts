import { createReducer, on } from '@ngrx/store';
import { loadRegistrations, loadRegistrationsSuccess } from './registration.actions';
import * as fromSelectors from './registration.selectors'

export const registrationsFeature = 'registration';

export const registrationsReducer = createReducer(
    fromSelectors.initialState,
    on(loadRegistrations, state => {
        return { ...state, items: state.items }
    }),
    on(loadRegistrationsSuccess, (state, { data }) => {
        return { ...state, items: data }
    })
  );