import { createSelector, createFeatureSelector } from '@ngrx/store';

export interface CourseState {
    items: any[];
}

export const initialState: CourseState = {
    items: []
};

export const selectRegistrations = createFeatureSelector('registrations')

export const selectRegistrationItems = createSelector(
    selectRegistrations,
    (state: CourseState) => {
        return state.items;
    }
);