import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegistrationRoutingModule } from './registration-routing.module';
import { IndexComponent } from './components/index/index.component';
import { EffectsModule } from '@ngrx/effects';
import { RegistrationEffects } from './registraton.effects';
import { StoreModule } from '@ngrx/store';
import * as fromReducers from './registration.reducers';

@NgModule({
  declarations: [
    
  
    IndexComponent
  ],
  imports: [
    CommonModule,
    RegistrationRoutingModule,
    StoreModule.forFeature(fromReducers.registrationsFeature, fromReducers.registrationsReducer),
    EffectsModule.forFeature([RegistrationEffects])
  ]
})
export class RegistrationModule { }
