import { createAction, props } from '@ngrx/store';

export const loadRegistrations = createAction(
  '[Registration] Load Registrations'
);

export const loadRegistrationsSuccess = createAction(
  '[Registration] Load Registrations Success',
  props<{ data: any }>()
);

export const loadRegistrationsFailure = createAction(
  '[Registration] Load Registrations Failure',
  props<{ error: any }>()
);
